# DatingApp

A practical example of how to build an application with ASP.NET Core WebAPI (v2.1) and Angular (v6) from start to finish

# Course

An Udemy course [Build an app with ASPNET Core and Angular from scratch](https://www.udemy.com/build-an-app-with-aspnet-core-and-angular-from-scratch/) by [Neil Cummings](https://www.udemy.com/user/neil-cummings-2/)

# Some of the skills this course teach
- Setting up the developer environment
- Creating the ASP.NET Core WebAPI and the [Angular](https://angular.io/) app using the DotNet CLI and the Angular CLI
- Adding a Client side login and register function to our [Angular](https://angular.io/) application
- Adding 3rd party components to add some pizzazz to the app
- Adding routing to the [Angular](https://angular.io/) application and securing routes.
- Using Automapper in ASP.NET Core
- Building a great looking UI using [Bootstrap 4](https://getbootstrap.com/)
- Adding Photo Upload functionality as well as a cool looking gallery in [Angular](https://angular.io/)
- [Angular](https://angular.io/) Template forms and Reactive forms and validation
- Paging, Sorting and Filtering
- Adding a Private Messaging system to the app
- Publishing the application to both IIS and Linux
- Many more things as well

# Runtime
Due to MySQL provider specific version `Pomelo.EntityFrameworkCore.MySql 2.1.1` we need to install version `SDK 2.1.301` from [here](https://www.microsoft.com/net/download/dotnet-core/2.1)

# Database
## Install Database
* MySQL/MariaDB
### Download
* [MariaDB Download Page](https://downloads.mariadb.org/)
* [MySQL Download Page](https://www.mysql.com/downloads/)
### In Development

#### Prepare database
* connect to mysql-cli
```powershell
PS> mysql -u root -p
```
* create new user
```sql
MariaDB> CREATE USER 'appuser_dev'@'localhost' IDENTIFIED BY 'password';
```
* grant privileges (NOT SAFE - use your own settings)
```sql
MariaDB> GRANT ALL PRIVILEGES ON *.* TO 'appuser_dev'@'localhost' WITH GRANT OPTION;
```
* Run commands from `DatingApp.API\` to create database and apply migrations
```powershell
$Env:ASPNETCORE_ENVIRONMENT = "Development"
dotnet ef database update
```
* Use seeder to add testing data into database
```
dotnet run --seed true
```

### In Production
* MySQL/MariaDB
#### Download
* [MariaDB Download Page](https://downloads.mariadb.org/)
* [MySQL Download Page](https://www.mysql.com/downloads/)
#### Prepare database
* connect to mysql-cli
```powershell
PS> mysql -u root -p
```
* create new user
```sql
MariaDB> CREATE USER 'appuser'@'localhost' IDENTIFIED BY 'password';
```
* grant privileges (NOT SAFE - use your own settings)
```sql
MariaDB> GRANT ALL PRIVILEGES ON *.* TO 'appuser'@'localhost' WITH GRANT OPTION;
```
* Run commands from `DatingApp.API\` to create database and apply migrations
```powershell
$Env:ASPNETCORE_ENVIRONMENT = "Production"
dotnet ef database update
```

# Dependencies

### SPA
* Run commands from `DatingApp-SPA\`
```powershell
npm install
```
### API
* Run commands from `DatingApp.API\`
```powershell
dotnet restore
```

# Build

## Angular (SPA)
* Run commands from `DatingApp-SPA\`
* built application will be in `DatingApp.API\wwwroot\`

### Development Build
```powershell
cd DatingApp-SPA
ng build
```
### Production Build
```powershell
cd DatingApp-SPA
ng build --prod --build-optimizer=false
```

## .NET Core (API)
* Run commands from `DatingApp.API\`
### Development
```powershell
$Env:ASPNETCORE_ENVIRONMENT = "Development"
dotnet restore
dotnet build
```
### Production
```powershell
$Env:ASPNETCORE_ENVIRONMENT = "Production"
dotnet restore
dotnet build
```

# Run

## Development
* first terminal window:
```powershell
cd DatingApp-SPA
ng serve
```
* second terminal window
```powershell
cd DatingApp.API
dotnet watch run
```

## Production
